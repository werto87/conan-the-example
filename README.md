# conan recipes

## add this project to your conan remotes
conan remote add gitlab https://gitlab.com/api/v4/projects/27217743/packages/conan

## if you like to build for emscripten

1. download the emscripten file from this repo
2. save it in your conan profile directory
3. set the correct profiles  --profile:build=< profile you use to build if you are not corss building > --profile:host=emscripten 

for example if you usaly build with the "clang" profile and want to create the magnum package:
conan create . magnum/2020.06@werto87/stable  --profile:build=clang --profile:host=emscripten --build missing
